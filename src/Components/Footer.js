import React, { Component } from 'react'

export default class Footer extends Component {
	render() {
		return (
			<div className='Footer'>
				<div className='Footer--title'>&copy; 2020 | Denny Angesti Pratama</div>
			</div>
		)
	}
}
