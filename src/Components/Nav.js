import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Nav extends Component {
	render() {
		return (
			<div className='Nav'>
				<div className='Nav--title'>Swapiku</div>
				<div className='Nav__List'>
					<div className='Nav__List--title'>
						<Link to='/movies'>Films</Link>
					</div>
					<div className='Nav__List--title'>
						<Link to='/'>People</Link>
					</div>
				</div>
			</div>
		)
	}
}
