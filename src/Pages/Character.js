import React, { Component, Fragment } from 'react'
import moment from 'moment'

import Nav from '../Components/Nav'
import Footer from '../Components/Footer'

const base_url = 'http://swapi.dev/api'
export default class Character extends Component {
	constructor(props) {
		super(props)
		console.log(props)
		this.state = {
			id: props.match.params.id,
			character: null,
			isLoading: false,
		}
	}

	componentDidMount() {
		this.fetchDetailCharacter()
	}

	fetchDetailCharacter = () => {
		const { id } = this.state
		this.setState({ isLoading: true }, () => {
			fetch(`${base_url}/people/${id}`)
				.then((response) => response.json())
				.then((data) => {
					this.setState({
						isLoading: false,
						character: data,
					})
				})
		})
	}

	render() {
		const { character, isLoading } = this.state
		return (
			<Fragment>
				<Nav />
				<div className='Character'>
					<div className='Home__Content__Card'>
						<div className='Home__Content__Card--title'>{isLoading ? null : character?.name}</div>
						<div className='Home__Content__Card__Wrapper'>
							<div className='Home__Content__Card__Wrapper--title'>Gender</div>
							<div className='Home__Content__Card__Wrapper--text'>{character?.gender}</div>
						</div>
						<div className='Home__Content__Card__Wrapper'>
							<div className='Home__Content__Card__Wrapper--title'>Created</div>
							<div className='Home__Content__Card__Wrapper--text'>
								{moment(character?.created).format('DD MMMM YYYY')}
							</div>
						</div>
						<div className='Home__Content__Card__Wrapper'>
							<div className='Home__Content__Card__Wrapper--title'>Birth Year</div>
							<div className='Home__Content__Card__Wrapper--text'>{character?.birth_year}</div>
						</div>
					</div>
				</div>
				<Footer />
			</Fragment>
		)
	}
}
