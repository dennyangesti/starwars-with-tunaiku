import React, { Component } from 'react'
import Skeleton from 'react-loading-skeleton'

import Nav from '../Components/Nav'
import Footer from '../Components/Footer'

const base_url = 'http://swapi.dev/api'
export default class Movies extends Component {
	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			movies: [],
		}
	}

	componentDidMount() {
		this.fetchListMovies()
	}

	fetchListMovies = () => {
		this.setState({ isLoading: true }, () => {
			fetch(`${base_url}/films/`)
				.then((response) => response.json())
				.then((data) => {
					this.setState({
						isLoading: false,
						movies: data.results,
					})
				})
		})
	}

	render() {
		const { isLoading, movies } = this.state
		return (
			<div className='Home'>
				<Nav />
				{isLoading ? (
					<div className='Loading__Card'>
						<Skeleton width={450} height={215} />
						<Skeleton width={450} height={215} />
						<Skeleton width={450} height={215} />
						<Skeleton width={450} height={215} />
					</div>
				) : (
					<div className='Home__Content'>
						{movies.map((movie, index) => {
							return (
								<div className='Home__Content__Card' key={index}>
									<div className='Home__Content__Card--title'>{movie.title}</div>
									<div className='Home__Content__Card__Wrapper'>
										<div className='Home__Content__Card__Wrapper--title'>Director</div>
										<div className='Home__Content__Card__Wrapper--text'>{movie.director}</div>
									</div>
									<div className='Home__Content__Card__Wrapper'>
										<div className='Home__Content__Card__Wrapper--title'>Producer</div>
										<div className='Home__Content__Card__Wrapper--text shorten'>{movie.producer}</div>
									</div>
								</div>
							)
						})}
					</div>
				)}

				<Footer />
			</div>
		)
	}
}
