import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Skeleton from 'react-loading-skeleton'

import Nav from '../Components/Nav'
import Footer from '../Components/Footer'

const base_url = 'http://swapi.dev/api'
export default class Home extends Component {
	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			characters: [],
		}
	}

	componentDidMount() {
		this.fetchListCharacters()
	}

	fetchListCharacters = () => {
		this.setState({ isLoading: true }, () => {
			fetch(`${base_url}/people/`)
				.then((response) => response.json())
				.then((data) => {
					this.setState({
						isLoading: false,
						characters: data.results,
					})
				})
		})
	}

	render() {
		const { characters, isLoading } = this.state
		return (
			<div className='Home'>
				<Nav />
				{isLoading ? (
					<div className='Loading__Card'>
						<Skeleton width={450} height={215} />
						<Skeleton width={450} height={215} />
						<Skeleton width={450} height={215} />
						<Skeleton width={450} height={215} />
					</div>
				) : (
					<div className='Home__Content'>
						{characters.map((character, index) => {
							return (
								<div className='Home__Content__Card' key={index}>
									<div className='Home__Content__Card--title'>{character.name}</div>
									<div className='Home__Content__Card__Wrapper'>
										<div className='Home__Content__Card__Wrapper--title'>Birth Year</div>
										<div className='Home__Content__Card__Wrapper--text'>{character.birth_year}</div>
									</div>
									<div className='Home__Content__Card__Wrapper'>
										<div className='Home__Content__Card__Wrapper--title'>Eye Color</div>
										<div className='Home__Content__Card__Wrapper--text'>{character.eye_color}</div>
									</div>
									<Link to={`/character/${index + 1}`}>
										<button>See More</button>
									</Link>
								</div>
							)
						})}
					</div>
				)}
				<Footer />
			</div>
		)
	}
}
