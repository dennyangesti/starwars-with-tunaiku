import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Home from './Pages/Home'
import Character from './Pages/Character'
import Movies from './Pages/Movies'

import './Styles/Main.scss'

export default class App extends Component {
	render() {
		return (
			<Router>
				<Switch>
					<Route exact path={'/'} component={Home} />
					<Route exact path={'/character/:id'} component={Character} />
					<Route exact path={'/movies'} component={Movies} />
				</Switch>
			</Router>
		)
	}
}
